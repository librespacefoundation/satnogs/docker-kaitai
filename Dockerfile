# SatNOGS Kaitai Struct Docker image
#
# Copyright (C) 2018-2021, 2024 Libre Space Foundation <https://libre.space/>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

ARG DEBIAN_IMAGE_TAG=bookworm
FROM debian:${DEBIAN_IMAGE_TAG}
LABEL org.opencontainers.image.authors='SatNOGS project <dev@satnogs.org>'

ARG KAITAI_VERSION

WORKDIR /workdir/

# Install 'kaitai-struct-compiler'
RUN apt-get update \
	&& apt-get install -qy curl \
	&& curl -sLO https://github.com/kaitai-io/kaitai_struct_compiler/releases/download/${KAITAI_VERSION}/kaitai-struct-compiler_${KAITAI_VERSION}_all.deb \
	&& apt-get install -qy ./kaitai-struct-compiler_${KAITAI_VERSION}_all.deb \
	&& rm kaitai-struct-compiler_${KAITAI_VERSION}_all.deb \
	&& rm -rf /var/lib/apt/lists/*

ENTRYPOINT ["/usr/bin/ksc"]
